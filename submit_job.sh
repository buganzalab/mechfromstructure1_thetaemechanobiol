#!/bin/bash

#SBATCH -A abuganza 
#SBATCH --nodes=1
#SBATCH --job-name=Wound
#SBATCH --time=24:00:00

## load general modules
module load rcac
module load gcc

## load modules needed for plotting using python matplolib
module use /depot/abuganza/etc/modules 
module load conda-env/CondaUQ-py3.8.5

## clean and make again using multi-threading
make clean
make -j24 apps/sim_wound_tri_sym

## run wound healing simulation
./apps/sim_wound_tri_sym meshing/abaqus_trimesh_transition.txt job/input.txt job/output

## use python to postprocess results and produce some charts
python postprocessor.py