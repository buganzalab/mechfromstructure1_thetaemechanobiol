/*
	Test biology.
	Simple main file to test the biology constitutive equations.
*/

#include "biology.h"
#include <vector>
#include <map>
#include <math.h>
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
using namespace Eigen;

double frand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main(int argc, char *argv[])
{

	std::cout<<"\nTesting biology constitutive eq. \n(call all the functions)\n";

	//************************************************************************************************************************//
	// INITIALIZE
	//************************************************************************************************************************//
  //---------------------------------//
  // GLOBAL PARAMETERS
  //
  // for normalization
  double rho_phys = 3700; // [cells/mm^3], from the IN VITRO assay
  double c_max = 1.0e-4; // [g/mm3] from tgf beta review, 5e-5g/mm3 was good for tissues, from the literature
  //
  double k0 = 10.0e-3; // neo hookean for skin, fit against IN VIVO data, in MPa
  double k2c = 21.3; // nonlinear exponential, fit against IN VIVO data
  double k2f = 5.52; // nonlinear exponential coefficient, non-dimensional, from IN VIVO data
  //
  // not thaking the following parameters into account for now
  double t_rho = 11.4e-6; // force of fibroblasts in MPa/(cell/mm^3)
  double t_rho_c = 4*t_rho; // force of myofibroblasts enhanced by chemical MPa/(cell/mm^3)
  double K_t_c = c_max/10.; // saturation of chemical on force. this can be calculated from steady state
  double D_rhorho = 0.0833; // diffusion of cells in [mm^2/hour]
  double D_rhoc = 1.66e-12/c_max/c_max; // diffusion of chemotactic gradient, an order of magnitude greater than random walk [mm^2/hour], not normalized
  double Fc_max = 0.3; // effect of the chemical concentration on the chemotactic response
  //
  double D_cc = 0.10; // diffusion of chemical TGF, not normalized, in [mm^2/hr]
  //
  double p_rho =0.22/24; // in 1/hour production of fibroblasts naturally, proliferation rate, not normalized, based on data of doubling rate from commercial use
  double p_rho_c = 2*p_rho; // production enhanced by the chem, if the chemical is normalized, then suggest two fold,
  double p_rho_theta = 0.0; // enhanced production by theta
  double K_rho_c= c_max/10.; // saturation of cell proliferation by chemical, this one is definitely not crucial, just has to be small enough <cmax
  double d_rho = 0.2*p_rho ;// decay of cells, 20 percent of cells die per day
  double K_rho_rho = 3700; // saturation of cell by cell, from steady state
  double vartheta_e = 2.0; // physiological state of area stretch
  double gamma_theta = 5.; // sensitivity of heviside function
  //
  double p_c_rho = 90.0e-16/rho_phys;// production of c by cells in g/cells/h
  double p_c_thetaE = 300.0e-16/rho_phys; // coupling of elastic and chemical, three fold
  double K_c_c = 1.;// saturation of chem by chem, from steady state
  double d_c = 0.01; // decay of chemical in 1/hours
  //---------------------------------//
  std::vector<double> global_parameters = {k0,k2c,k2f,t_rho,t_rho_c,K_t_c,D_rhorho,D_rhoc,Fc_max,D_cc,p_rho,p_rho_c,p_rho_theta,K_rho_c,K_rho_rho,d_rho,vartheta_e,gamma_theta,p_c_rho,p_c_thetaE,K_c_c,d_c};

  //---------------------------------//
  // LOCAL PARAMETERS
  //
  // fiber alignment
  double tau_omega_c = 100; // time constant for angular reorientation of collagen
  double tau_omega_f = 100; // time constant for angular reorientation of fibronectin
  //
  // dispersion parameter
  double tau_kappa_c   = 10; // time constant
  double gamma_kappa_c = 5; // exponent of the principal stretch ratio
  double tau_kappa_f   = 10; // time constant
  double gamma_kappa_f = 5; // exponent of the principal stretch ratio
  //
  // permanent contracture/growth
  double tau_lamdaP_a = 1000; // time constant for direction a, from estimates of pig growth in response to strain
  double tau_lamdaP_s = 1000; // time constant for direction s, from estimated of pig skin response to strain
  //
  double thick_0  =  5; // reference thickness in mm
  double phic_vol =  1e-16; // volume of collagen associated to healthy collagen volume fraction.
  double phif_vol =  1e-16; // volume ot fibronectin associated to healt
  //
  std::vector<double> local_parameters = {tau_omega_c,tau_omega_f,tau_kappa_c,gamma_kappa_c,tau_kappa_f,gamma_kappa_f,tau_lamdaP_a,tau_lamdaP_s,thick_0,phic_vol,phif_vol};
  //
	// define variables used inside of the biology
	//
	Matrix2d FF; FF.setZero(); FF(0,0)=1.0*frand(1.,1.5); FF(1,1) = 2.0*frand(1.,1.5);// deformation gradient
	double rho = 10000.0*frand(0.7,1.1); // cell density in cells/mm^2
	double c = 1e-5; // chemical concentration in g/mm^3
	Vector2d Grad_rho; Grad_rho.setZero(); Grad_rho(0) = 2.0*frand(0.,1.); Grad_rho(1) = 1.0*frand(0.,1.);// gradient of cell
	Vector2d Grad_c; Grad_c.setZero(); Grad_c(0) = 0.1*frand(0.,1.); Grad_c(1) = 0.1*frand(0.,1.); // gradient of chem
	double phic = 1.0; // collagen content in normalized fraction (i.e. wrt healthy, no units and [0,1])
	double kc = 0.015; // collagen stiffness in MPa
	Vector2d a0c;a0c.setZero(); a0c(0)=1.0*frand(0.,1.0); a0c(1) = sqrt(1-a0c(0)*a0c(0));  // collagen direction
	double kappac = 0.5; // collagen dispersion
	double phif = 1.0; // fibronectin content in normalized fraction (i.e., no units, wrt healthy, [0,1])
	double kf = 0.001; // fibronectin stiffness in MPa
	Vector2d a0f;a0f.setZero(); a0f(0)=1.0*frand(0.,1.0); a0f(1) = sqrt(1-a0f(0)*a0f(0)); // fibronectin direction
	double kappaf = 0.1; // fibronectin dispersion
	Vector2d lamdaP; lamdaP.setZero();lamdaP(0)=1.0;lamdaP(1)=2.0; // growth
	std::cout<<"Input values\n FF\n "<<FF<<"\n rho = "<<rho<<"\n c = "<<c<<"\n Grad_rho\n "<<Grad_rho<<"\n Grad_c\n "<<Grad_c;
	std::cout<<"\n phic = "<<phic<<"\n kc = "<<kc<<"\n kappac = "<<kappac<<"\n a0c\n "<<a0c;
	std::cout<<"\n phif = "<<phif<<"\n kf = "<<kf<<"\n kappaf = "<<kappaf<<"\n a0f\n "<<a0f;
	std::cout<<"\n lamdaP\n "<<lamdaP<<"\n";

	// some other global variables
	//
	double epsilon = 1e-7; // for numerical derivatives
	Matrix2d CC = FF.transpose()*FF;
	// polar decomposition
	EigenSolver<Matrix2d> es(CC);
	std::cout<<"the eigenvalues of CC are \n"<<es.eigenvalues()<<"\n";
	std::cout<<"the matrix of eigenvectors is \n"<<es.eigenvectors()<<"\n";
	// then polar decomposition is
	std::vector<Vector2d> Ubasis(2,Vector2d(0.0,0.0));
	Matrix2d CC_eiv = es.eigenvectors().real();
	std::cout<<"or in double casting form\n"<<CC_eiv<<"\n";
	Ubasis[0] =  CC_eiv.col(0);
	Ubasis[1] =  CC_eiv.col(1);
	Vector2d Clamda =  es.eigenvalues().real();
	// Build the U mnatrix from basus and the root of the iegenvuales
	Matrix2d UU = sqrt(Clamda(0))*Ubasis[0]*Ubasis[0].transpose() + sqrt(Clamda(1))*Ubasis[1]*Ubasis[1].transpose() ;
	Matrix2d RR = FF*UU.inverse();
	// just quick check
	std::cout<<"FF\n"<<FF<<"\nRR\n"<<RR<<"\nUU\n"<<UU<<"\nRR*UU\n"<<RR*UU<<"\n";

	//************************************************************************************************************************//
	// GLOBAL EQUATIONS
	//************************************************************************************************************************//

/*
	//-------------------------------//
	// Active stress
	//------------------------------//

	// Active stress (first overload)
	//
	Matrix2d SSact_test1 = evalSS_act(FF, rho, Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif,  kf, a0f,  kappaf,lamdaP );

	// Active stress (second overload)
	//
	Matrix2d SSact_test2;
	std::vector<double> dSS_actdCC_explicit(16,0.0);
	Matrix2d dSS_actdrho_explicit;dSS_actdrho_explicit.setZero();
	Matrix2d dSS_actdc_explicit; dSS_actdc_explicit.setZero();
	evalSS_act(FF, rho, Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif,  kf, a0f,  kappaf,lamdaP, SSact_test2, dSS_actdCC_explicit, dSS_actdrho_explicit, dSS_actdc_explicit);

	//-------------------------------//
	// Cell flux (migration)
	//-------------------------------//

	// First overload
	Vector2d Qrho_t = evalQ_rho(FF, rho, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP,global_parameters);

	// Second overload, Flux of rho and Tangent
	Vector2d Q_rho_t2;
	Matrix2d dQ_rhoxdCC_e,dQ_rhoydCC_e;
	Vector2d dQ_rhodrho_e,dQ_rhodc_e;
	Matrix2d dQ_rhodGradrho,dQ_rhodGradc;
	evalQ_rho( FF,  rho,  Grad_rho,  c,   Grad_c,  phic, kc,  a0c, kappac, phif, kf,a0f,  kappaf,lamdaP, global_parameters, Q_rho_t2, dQ_rhoxdCC_e, dQ_rhoydCC_e, dQ_rhodrho_e, dQ_rhodGradrho, dQ_rhodc_e, dQ_rhodGradc);

	//-------------------------------//
	// Chemical flux (diffusion)
	//-------------------------------//

	// First overload
	Vector2d Qc_t = evalQ_c(FF, rho, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);

	// Second overload, Flux of rho and Tangent
	Vector2d Q_c_t2;
	Matrix2d dQ_cxdCC_e,dQ_cydCC_e;
	Vector2d dQ_cdrho_e,dQ_cdc_e;
	Matrix2d dQ_cdGradrho,dQ_cdGradc;
	evalQ_c( FF,  rho,  Grad_rho,  c,   Grad_c,  phic, kc,  a0c, kappac, phif, kf,a0f,  kappaf,lamdaP, Q_c_t2, dQ_cxdCC_e, dQ_cydCC_e, dQ_cdrho_e, dQ_cdGradrho, dQ_cdc_e, dQ_cdGradc);

	//-------------------------------//
	// Cell source (proliferation)
	//-------------------------------//

	// source of rho
	double S_rho_t1 = evalS_rho(FF, rho, Grad_rho, c,  Grad_c, phic, kc,  a0c, kappac, phif, kf,  a0f,kappaf, lamdaP);

	// source of rho for tangent
	double S_rho_t2, dSrhodrho_explicit, dSrhodc_explicit;
	Matrix2d dSrhodCC_explicit;
	evalS_rho(FF, rho,  Grad_rho, c,   Grad_c, phic, kc,   a0c, kappac, phif, kf,   a0f, kappaf,  lamdaP, S_rho_t2, dSrhodCC_explicit, dSrhodrho_explicit, dSrhodc_explicit);

	//-------------------------------//
	// Chemical source (production)
	//-------------------------------//
	// source of chemical
	double S_c_t1 = evalS_c(FF,  rho, Grad_rho, c, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);

	// source of the chemical for the tangent
	double S_c_t2, dScdrho_explicit, dScdc_explicit;
	Matrix2d dScdCC_explicit;
	evalS_c(FF,  rho,Grad_rho, c,Grad_c, phic, kc, a0c, kappac,phif, kf,a0f, kappaf,lamdaP,S_c_t2, dScdCC_explicit,dScdrho_explicit, dScdc_explicit);

	//************************************************************************************************************************ //
	// LOCAL EQUATIONS
	//************************************************************************************************************************ //

	//-------------------------------//
	// collagen production
	//-------------------------------//
	// production first overload
	double phicdotpl_1 = eval_phic_dot_plus(CC, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double phicdotpl_2,dphicdotplusdrho,dphicdotplusdc;
	Matrix2d dphicdotplusdCC;
	// product second overload
	eval_phic_dot_plus(CC, rho, c, phic, kc,a0c, kappac, phif, kf,a0f,  kappaf, lamdaP, phicdotpl_2, dphicdotplusdCC,dphicdotplusdrho, dphicdotplusdc);
	// degradation first overload
	double phicdotmi_1 = eval_phic_dot_minus(CC, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double phicdotmi_2,dphicdotminusdrho,dphicdotminusdc;
	Matrix2d dphicdotminusdCC;
	// degradation second overload
	eval_phic_dot_minus(CC, rho, c, phic, kc,a0c, kappac, phif, kf,a0f,  kappaf, lamdaP, phicdotmi_2, dphicdotminusdCC,dphicdotminusdrho, dphicdotminusdc);

	//-------------------------------//
	// fibronectin production
	//-------------------------------//
	// production first overload
	double phifdotpl_1 = eval_phif_dot_plus(CC, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double phifdotpl_2,dphifdotplusdrho,dphifdotplusdc;
	Matrix2d dphifdotplusdCC;
	// product second overload
	eval_phif_dot_plus(CC, rho, c, phic, kc,a0c, kappac, phif, kf,a0f,  kappaf, lamdaP, phifdotpl_2, dphifdotplusdCC,dphifdotplusdrho, dphifdotplusdc);
	// degradation first overload
	double phifdotmi_1 = eval_phif_dot_minus(CC, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double phifdotmi_2,dphifdotminusdrho,dphifdotminusdc;
	Matrix2d dphifdotminusdCC;
	// degradation second overload
	eval_phif_dot_minus(CC, rho, c, phic, kc,a0c, kappac, phif, kf,a0f,  kappaf, lamdaP, phifdotmi_2, dphifdotminusdCC,dphifdotminusdrho, dphifdotminusdc);


	//-------------------------------//
	// NUMERICAL CHECKS
	//-------------------------------//

	// Check the derivatives numerically
	//
	double rho_plus = rho + epsilon;
	double rho_minus = rho - epsilon;
	double c_plus = c + epsilon;
	double c_minus = c - epsilon;

	// CC derivative

	Matrix2d FF_plus,FF_minus;
	Matrix2d CC_plus,CC_minus;
	Matrix2d UU_plus,UU_minus;
	std::vector<Vector2d> Ebasis(2,Vector2d(0.0,0.0));
	Ebasis[0] = Vector2d(1.0,0.0);
	Ebasis[1] = Vector2d(0.0,1.0);
	std::vector<Vector2d> Ubasis_plus(2,Vector2d(0.0,0.0));
	std::vector<Vector2d> Ubasis_minus(2,Vector2d(0.0,0.0));
	Matrix2d CC_eiv_plus,CC_eiv_minus;
	Vector2d Clamda_plus,Clamda_minus;
	//
	Matrix2d SS_act_pFF,SS_act_mFF;
	std::vector<double> dSS_actdCC_num(16,0.0);
	//
	Vector2d Qrho_pFF, Qrho_mFF;
	Matrix2d dQrhoxdCC_num,dQrhoydCC_num;
	std::vector<std::vector<double> > Q_rho_all_plus;
	Q_rho_all_plus.push_back(std::vector<double> {0.}); Q_rho_all_plus.push_back(std::vector<double> {0., 0.}); Q_rho_all_plus.push_back(std::vector<double> {0.});
	std::vector<std::vector<double> > Q_rho_all_minus;
	Q_rho_all_minus.push_back(std::vector<double> {0.}); Q_rho_all_minus.push_back(std::vector<double> {0., 0.}); Q_rho_all_minus.push_back(std::vector<double> {0.});
	Matrix2d dgradcnormdCC;
	//
	Vector2d Qc_pFF, Qc_mFF;
	Matrix2d dQcxdCC_num,dQcydCC_num;
	//
	double S_rho_pFF,S_rho_mFF;
	Matrix2d dSrhodCC_num;
	//
	double S_c_pFF,S_c_mFF;
	Matrix2d dScdCC_num;
	//
	double phicdotpl_pCC,phicdotpl_mCC,phicdotmi_pCC,phicdotmi_mCC;
	Matrix2d dphicdotplusdCC_num, dphicdotminusdCC_num;
	//
	double phifdotpl_pCC,phifdotpl_mCC,phifdotmi_pCC,phifdotmi_mCC;
	Matrix2d dphifdotplusdCC_num, dphifdotminusdCC_num;
	//
	for(int coordk = 0;coordk<2;coordk++){
		for(int coordl = 0;coordl<2;coordl++){
			CC_plus = CC + epsilon*(Ebasis[coordk]*Ebasis[coordl].transpose())+ epsilon*(Ebasis[coordl]*Ebasis[coordk].transpose());
			CC_minus = CC - epsilon*(Ebasis[coordk]*Ebasis[coordl].transpose())- epsilon*(Ebasis[coordl]*Ebasis[coordk].transpose());
			// Polar decomposition of CCplus and CCminus
			EigenSolver<Matrix2d> esp(CC_plus);
			EigenSolver<Matrix2d> esm(CC_minus);
			Matrix2d CC_eiv_plus = esp.eigenvectors().real();
			Matrix2d CC_eiv_minus = esm.eigenvectors().real();
			Ubasis_plus[0] =  CC_eiv_plus.col(0);
			Ubasis_plus[1] =  CC_eiv_plus.col(1);
			Ubasis_minus[0] =  CC_eiv_minus.col(0);
			Ubasis_minus[1] =  CC_eiv_minus.col(1);
			Clamda_plus =  esp.eigenvalues().real();
			Clamda_minus =  esm.eigenvalues().real();
			UU_plus = sqrt(Clamda_plus(0))*Ubasis_plus[0]*Ubasis_plus[0].transpose() + sqrt(Clamda_plus(1))*Ubasis_plus[1]*Ubasis_plus[1].transpose() ;
			UU_minus = sqrt(Clamda_minus(0))*Ubasis_minus[0]*Ubasis_minus[0].transpose() + sqrt(Clamda_minus(1))*Ubasis_minus[1]*Ubasis_minus[1].transpose() ;
			FF_plus = RR*UU_plus;
			FF_minus = RR*UU_minus;

			//--------------------//
			// Global eq
			//--------------------//

			// SSact
			SS_act_pFF = evalSS_act(FF_plus, rho, Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif,  kf, a0f,  kappaf,lamdaP );
			SS_act_mFF = evalSS_act(FF_minus, rho, Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif,  kf, a0f,  kappaf,lamdaP );
			for(int coordi = 0;coordi<2;coordi++){
				for(int coordj = 0;coordj<2;coordj++){
					dSS_actdCC_num[coordi*8+coordj*4+coordk*2+coordl] = (1.0/(4.0*epsilon))*(SS_act_pFF(coordi,coordj) - SS_act_mFF(coordi,coordj) );
				}
			}

			// Q_rho
			Qrho_pFF = evalQ_rho(FF_plus, rho, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
			Qrho_mFF = evalQ_rho(FF_minus, rho, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
			dQrhoxdCC_num(coordk,coordl) = (1.0/(4.0*epsilon))*(Qrho_pFF(0) - Qrho_mFF(0) );
			dQrhoydCC_num(coordk,coordl) = (1.0/(4.0*epsilon))*(Qrho_pFF(1) - Qrho_mFF(1) );
			// intermediate steps in the Q_rho
			evalQ_rho(FF_plus, rho, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP,  Q_rho_all_plus);
			evalQ_rho(FF_minus, rho, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP, Q_rho_all_minus);
			dgradcnormdCC(coordk,coordl) = (1.0/(4.0*epsilon))*(Q_rho_all_plus[0][0] - Q_rho_all_minus[0][0] );

			// Q_c
			Qc_pFF = evalQ_c(FF_plus, rho, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
			Qc_mFF = evalQ_c(FF_minus, rho, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
			dQcxdCC_num(coordk,coordl) = (1.0/(4.0*epsilon))*(Qc_pFF(0) - Qc_mFF(0) );
			dQcydCC_num(coordk,coordl) = (1.0/(4.0*epsilon))*(Qc_pFF(1) - Qc_mFF(1) );

			// S_rho
			S_rho_pFF = evalS_rho(FF_plus,  rho, Grad_rho, c, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
			S_rho_mFF = evalS_rho(FF_minus,  rho, Grad_rho, c, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
			dSrhodCC_num(coordk,coordl) = (1.0/(4.0*epsilon))*(S_rho_pFF - S_rho_mFF);

			// S_c
			S_c_pFF = evalS_c(FF_plus,  rho, Grad_rho, c, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
			S_c_mFF = evalS_c(FF_minus,  rho, Grad_rho, c, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
			dScdCC_num(coordk,coordl) = (1.0/(4.0*epsilon))*(S_c_pFF - S_c_mFF);

			//--------------------//
			// Local eq
			//--------------------//

			// collagen production and degradation
			phicdotpl_pCC = eval_phic_dot_plus(CC_plus, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
			phicdotpl_mCC = eval_phic_dot_plus(CC_minus, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
			phicdotmi_pCC = eval_phic_dot_minus(CC_plus, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
			phicdotmi_mCC = eval_phic_dot_minus(CC_minus, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
			dphicdotplusdCC_num(coordk,coordl) = (1.0/(4.0*epsilon))*(phicdotpl_pCC - phicdotpl_mCC);
			dphicdotminusdCC_num(coordk,coordl) = (1.0/(4.0*epsilon))*(phicdotmi_pCC - phicdotmi_mCC);

			// fibronectin production and degradation
			phifdotpl_pCC = eval_phif_dot_plus(CC_plus, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
			phifdotpl_mCC = eval_phif_dot_plus(CC_minus, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
			phifdotmi_pCC = eval_phif_dot_minus(CC_plus, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
			phifdotmi_mCC = eval_phif_dot_minus(CC_minus, rho, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
			dphifdotplusdCC_num(coordk,coordl) = (1.0/(4.0*epsilon))*(phifdotpl_pCC - phifdotpl_mCC);
			dphifdotminusdCC_num(coordk,coordl) = (1.0/(4.0*epsilon))*(phifdotmi_pCC - phifdotmi_mCC);

		}
	}

	// rho derivative

	// GLOBAL

	// SSact
	Matrix2d SSact_prho = evalSS_act(FF, rho_plus, Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif,  kf, a0f,  kappaf,lamdaP );
	Matrix2d SSact_mrho = evalSS_act(FF, rho_minus, Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif,  kf, a0f,  kappaf,lamdaP );
	Matrix2d dSS_actdrho_num = (1.0/(2.0*epsilon))*(SSact_prho-SSact_mrho);

	// Q_rho
	Vector2d Qrho_pl = evalQ_rho(FF, rho_plus, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d Qrho_mi = evalQ_rho(FF, rho_minus, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d dQrhodrho_num = (1.0/(2.0*epsilon))*(Qrho_pl - Qrho_mi);

	// Q_c
	Vector2d Qc_pl = evalQ_c(FF, rho_plus, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d Qc_mi = evalQ_c(FF, rho_minus, Grad_rho, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d dQcdrho_num = (1.0/(2.0*epsilon))*(Qc_pl - Qc_mi);

	// S_rho
	double S_rho_p = evalS_rho(FF,  rho_plus, Grad_rho, c, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
	double S_rho_m = evalS_rho(FF,  rho_minus, Grad_rho, c, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
	double dSrhodrho_num = (1.0/(2.0*epsilon))*(S_rho_p - S_rho_m);

	// S_c
	double S_c_p = evalS_c(FF,  rho_plus, Grad_rho, c, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
	double S_c_m = evalS_c(FF,  rho_minus, Grad_rho, c, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
	double dScdrho_num = (1.0/(2.0*epsilon))*(S_c_p - S_c_m);

	// LOCAL

	// collagen
	double phicdotpl_p = eval_phic_dot_plus(CC, rho_plus, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double phicdotpl_m = eval_phic_dot_plus(CC, rho_minus, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double dphicdotplusdrho_num =  (1.0/(2.0*epsilon))*(phicdotpl_p - phicdotpl_m);
	double phicdotmi_p = eval_phic_dot_minus(CC, rho_plus, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double phicdotmi_m = eval_phic_dot_minus(CC, rho_minus, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double dphicdotminusdrho_num =  (1.0/(2.0*epsilon))*(phicdotmi_p - phicdotmi_m);

	// fibronectin
	double phifdotpl_p = eval_phif_dot_plus(CC, rho_plus, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double phifdotpl_m = eval_phif_dot_plus(CC, rho_minus, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double dphifdotplusdrho_num =  (1.0/(2.0*epsilon))*(phifdotpl_p - phifdotpl_m);
	double phifdotmi_p = eval_phif_dot_minus(CC, rho_plus, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double phifdotmi_m = eval_phif_dot_minus(CC, rho_minus, c, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double dphifdotminusdrho_num =  (1.0/(2.0*epsilon))*(phifdotmi_p - phifdotmi_m);

	// c derivtive

	// GLOBAL

	// SSact
	Matrix2d SSact_pc = evalSS_act(FF, rho, Grad_rho, c_plus, Grad_c, phic, kc, a0c, kappac, phif,  kf, a0f,  kappaf,lamdaP );
	Matrix2d SSact_mc = evalSS_act(FF, rho, Grad_rho, c_minus, Grad_c, phic, kc, a0c, kappac, phif,  kf, a0f,  kappaf,lamdaP );
	Matrix2d dSS_actdc_num = (1.0/(2.0*epsilon))*(SSact_pc-SSact_mc);

	// Qrho
	Qrho_pl = evalQ_rho(FF, rho, Grad_rho, c_plus, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Qrho_mi = evalQ_rho(FF, rho, Grad_rho, c_minus, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d dQrhodc_num = (1.0/(2.0*epsilon))*(Qrho_pl - Qrho_mi);

	// Qc
	Qc_pl = evalQ_c(FF, rho, Grad_rho, c_plus, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Qc_mi = evalQ_c(FF, rho, Grad_rho, c_minus, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d dQcdc_num = (1.0/(2.0*epsilon))*(Qc_pl - Qc_mi);

	// S_rho
	S_rho_p = evalS_rho(FF,  rho, Grad_rho, c_plus, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
	S_rho_m = evalS_rho(FF,  rho, Grad_rho, c_minus, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
	double dSrhodc_num = (1.0/(2.0*epsilon))*(S_rho_p - S_rho_m);

	// S_c
	S_c_p = evalS_c(FF,  rho, Grad_rho, c_plus, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
	S_c_m = evalS_c(FF,  rho, Grad_rho, c_minus, Grad_c,  phic, kc,a0c, kappac, phif,  kf,a0f, kappaf,lamdaP);
	double dScdc_num = (1.0/(2.0*epsilon))*(S_c_p - S_c_m);

	// LOCAL

	// collagen
	phicdotpl_p = eval_phic_dot_plus(CC, rho, c_plus, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	phicdotpl_m = eval_phic_dot_plus(CC, rho, c_minus, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double dphicdotplusdc_num =  (1.0/(2.0*epsilon))*(phicdotpl_p - phicdotpl_m);
	phicdotmi_p = eval_phic_dot_minus(CC, rho, c_plus, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	phicdotmi_m = eval_phic_dot_minus(CC, rho, c_minus, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double dphicdotminusdc_num =  (1.0/(2.0*epsilon))*(phicdotmi_p - phicdotmi_m);

	// fibronectin
	phifdotpl_p = eval_phif_dot_plus(CC, rho, c_plus, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	phifdotpl_m = eval_phif_dot_plus(CC, rho, c_minus, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double dphifdotplusdc_num =  (1.0/(2.0*epsilon))*(phifdotpl_p - phifdotpl_m);
	phifdotmi_p = eval_phif_dot_minus(CC, rho, c_plus, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	phifdotmi_m = eval_phif_dot_minus(CC, rho, c_minus, phic, kc, a0c, kappac, phif, kf,a0f, kappaf,lamdaP);
	double dphifdotminusdc_num =  (1.0/(2.0*epsilon))*(phifdotmi_p - phifdotmi_m);

	// Grad_rho derivative

	Vector2d Grad_rho_px = Grad_rho + epsilon*Ebasis[0];
	Vector2d Grad_rho_py = Grad_rho + epsilon*Ebasis[1];
	Vector2d Grad_rho_mx = Grad_rho - epsilon*Ebasis[0];
	Vector2d Grad_rho_my = Grad_rho - epsilon*Ebasis[1];

	// Q_rho
	Vector2d Qrho_plx = evalQ_rho(FF, rho, Grad_rho_px, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d Qrho_mix = evalQ_rho(FF, rho, Grad_rho_mx, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d Qrho_ply = evalQ_rho(FF, rho, Grad_rho_py, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d Qrho_miy = evalQ_rho(FF, rho, Grad_rho_my, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Matrix2d dQ_rhodGradrho_num;
	dQ_rhodGradrho_num.col(0)= (1.0/(2.0*epsilon))*(Qrho_plx-Qrho_mix);
	dQ_rhodGradrho_num.col(1)= (1.0/(2.0*epsilon))*(Qrho_ply-Qrho_miy);

	// Q_c
	Vector2d Qc_plx = evalQ_c(FF, rho, Grad_rho_px, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d Qc_mix = evalQ_c(FF, rho, Grad_rho_mx, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d Qc_ply = evalQ_c(FF, rho, Grad_rho_py, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Vector2d Qc_miy = evalQ_c(FF, rho, Grad_rho_my, c, Grad_c,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Matrix2d dQ_cdGradrho_num;
	dQ_cdGradrho_num.col(0)= (1.0/(2.0*epsilon))*(Qc_plx-Qc_mix);
	dQ_cdGradrho_num.col(1)= (1.0/(2.0*epsilon))*(Qc_ply-Qc_miy);

	// Grad_c derivative

	Vector2d Grad_c_px = Grad_c + epsilon*Ebasis[0];
	Vector2d Grad_c_py = Grad_c + epsilon*Ebasis[1];
	Vector2d Grad_c_mx = Grad_c - epsilon*Ebasis[0];
	Vector2d Grad_c_my = Grad_c - epsilon*Ebasis[1];

	// Q_rho
	Qrho_plx = evalQ_rho(FF, rho, Grad_rho, c, Grad_c_px,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Qrho_mix = evalQ_rho(FF, rho, Grad_rho, c, Grad_c_mx,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Qrho_ply = evalQ_rho(FF, rho, Grad_rho, c, Grad_c_py,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Qrho_miy = evalQ_rho(FF, rho, Grad_rho, c, Grad_c_my,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Matrix2d dQ_rhodGradc_num;
	dQ_rhodGradc_num.col(0)= (1.0/(2.0*epsilon))*(Qrho_plx-Qrho_mix);
	dQ_rhodGradc_num.col(1)= (1.0/(2.0*epsilon))*(Qrho_ply-Qrho_miy);

	// Q_c
	Qc_plx = evalQ_c(FF, rho, Grad_rho, c, Grad_c_px,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Qc_mix = evalQ_c(FF, rho, Grad_rho, c, Grad_c_mx,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Qc_ply = evalQ_c(FF, rho, Grad_rho, c, Grad_c_py,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Qc_miy = evalQ_c(FF, rho, Grad_rho, c, Grad_c_my,  phic,  kc,  a0c,  kappac, phif,  kf,  a0f,  kappaf, lamdaP);
	Matrix2d dQ_cdGradc_num;
	dQ_cdGradc_num.col(0)= (1.0/(2.0*epsilon))*(Qc_plx-Qc_mix);
	dQ_cdGradc_num.col(1)= (1.0/(2.0*epsilon))*(Qc_ply-Qc_miy);
*/
	//-------------------------------//
	// PRINTING RESULTS
	//-------------------------------//

	// GLOBAL

	// SSact
	/*std::cout<<"\nSSact(), first overload\nSSact\n"<<SSact_test1<<"\n";
	std::cout<<"SSact(), second overload\nSSact\n"<<SSact_test2<<"\n";
	std::cout<<"dSS_actdrho_explicit\n"<<dSS_actdrho_explicit<<"\ndSS_actdrho_num\n "<<dSS_actdrho_num<<"\n";
	std::cout<<"dSS_actdc_explicit\n"<<dSS_actdc_explicit<<"\n dSS_actdc_num\n"<<dSS_actdc_num<<"\n";
	std::cout<<"dSS_actdCC_explicit\n";
	for(int i=0;i<16;i++)
	{
		std::cout<<dSS_actdCC_explicit[i]<<", ";
	}
	std::cout<<"\n";
	std::cout<<"dSS_actdCC_num\n";
	for(int i=0;i<16;i++)
	{
		std::cout<<dSS_actdCC_num[i]<<", ";
	}
	std::cout<<"\n";	*/
/*
	// Q_rho
	std::cout<<"\n\nCell flux\nQrho_t1\n"<<Qrho_t<<"\nQrho_t2\n"<<Q_rho_t2<<"\n";
	std::cout<<"dQrho_x_dCC\n"<<dQ_rhoxdCC_e<<"\ndQrhoxdCC_num\n"<<dQrhoxdCC_num<<"\ndQrho_y_dCC\n"<<dQ_rhoydCC_e<<"\ndQrhoydCC_num\n"<<dQrhoydCC_num<<"\n";
	std::cout<<"dQ_rhodrho\n"<<dQ_rhodrho_e<<"\ndQrhodrho_num\n"<<dQrhodrho_num<<"\ndQrhodGradrho\n"<<dQ_rhodGradrho<<"\ndQrhodGradrho_num\n"<<dQ_rhodGradrho_num<<"\n";
	std::cout<<"dQrhodc\n"<<dQ_rhodc_e<<"\ndQrhodc_num\n"<<dQrhodc_num<<"\ndQrhodGradc\n"<<dQ_rhodGradc<<"\ndQrhodGradc_num\n"<<dQ_rhodGradc_num<<"\n";
	std::cout<<"dgradcnormdCC_num\n"<<dgradcnormdCC<<"\n";
	// Q_c
	std::cout<<"\n\nChemical flux\nQc_t1\n"<<Qc_t<<"\nQc_t2\n"<<Q_c_t2<<"\n";
	std::cout<<"dQc_x_dCC\n"<<dQ_cxdCC_e<<"\ndQcxdCC_num\n"<<dQcxdCC_num<<"\ndQc_y_dCC\n"<<dQ_cydCC_e<<"\ndQcydCC_num\n"<<dQcydCC_num<<"\n";
	std::cout<<"dQ_cdrho\n"<<dQ_cdrho_e<<"\ndQcdrho_num\n"<<dQcdrho_num<<"\ndQcdGradrho\n"<<dQ_cdGradrho<<"\ndQcdGradrho_num\n"<<dQ_cdGradrho_num<<"\n";
	std::cout<<"dQcdc\n"<<dQ_cdc_e<<"\ndQcdc_num\n"<<dQcdc_num<<"\ndQcdGradc\n"<<dQ_cdGradc<<"\ndQcdGradc_num\n"<<dQ_cdGradc_num<<"\n";

	// S_rho
	std::cout<<"\n\nCell source\nSrho_t1\n"<<S_rho_t1<<"\nSrho_t2\n"<<S_rho_t2<<"\n";
	std::cout<<"dSrho_dCC\n"<<dSrhodCC_explicit<<"\ndSrhodCC_num\n"<<dSrhodCC_num<<"\n";
	std::cout<<"dS_rhodrho\n"<<dSrhodrho_explicit<<"\ndSrhodrho_num\n"<<dSrhodrho_num<<"\n";
	std::cout<<"dS_rhodc\n"<<dSrhodc_explicit<<"\ndSrhodc_num\n"<<dSrhodc_num<<"\n";

	// S_c
	std::cout<<"\n\nChemical source\nSc_t1\n"<<S_c_t1<<"\nSc_t2\n"<<S_c_t2<<"\n";
	std::cout<<"dSc_dCC\n"<<dScdCC_explicit<<"\ndScdCC_num\n"<<dScdCC_num<<"\n";
	std::cout<<"dS_cdrho\n"<<dScdrho_explicit<<"\ndScdrho_num\n"<<dScdrho_num<<"\n";
	std::cout<<"dS_cdc\n"<<dScdc_explicit<<"\ndScdc_num\n"<<dScdc_num<<"\n";

	// LOCAL
  */
/*
	// collagen
	std::cout<<"\n\nCollagen production\nphicdotpl_1\n"<<phicdotpl_1<<"\nphicdotpl_2\n"<<phicdotpl_2<<"\n";
	std::cout<<"dphicdotplusdrho\n"<<dphicdotplusdrho<<"\ndphicdotplusdrho_num\n"<<dphicdotplusdrho_num<<"\ndphicdotplusdc\n"<<dphicdotplusdc<<"\ndphicdotplusdc_num\n"<<dphicdotplusdc_num<<"\n";
	std::cout<<"dphicdotplusdCC\n"<<dphicdotplusdCC<<"\ndphicdotplusdCC_num\n"<<dphicdotplusdCC_num<<"\n";
	std::cout<<"\n\nCollagen degradation\nphicdotmi_1\n"<<phicdotmi_1<<"\nphicdotmi_2\n"<<phicdotmi_2<<"\n";
	std::cout<<"dphicdotminusdrho\n"<<dphicdotminusdrho<<"\ndphicdotminusdrho_num\n"<<dphicdotminusdrho_num<<"\ndphicdotminusdc\n"<<dphicdotminusdc<<"\ndphicdotminusdc_num\n"<<dphicdotminusdc_num<<"\n";
	std::cout<<"dphicdotminusdCC\n"<<dphicdotminusdCC<<"\ndphicdotminusdCC_num\n"<<dphicdotminusdCC_num<<"\n";

	// fibronectin
	std::cout<<"\n\nFibronectin production\nphifdotpl_1\n"<<phifdotpl_1<<"\nphifdotpl_2\n"<<phifdotpl_2<<"\n";
	std::cout<<"dphifdotplusdrho\n"<<dphifdotplusdrho<<"\ndphifdotplusdrho_num\n"<<dphifdotplusdrho_num<<"\ndphifdotplusdc\n"<<dphifdotplusdc<<"\ndphifdotplusdc_num\n"<<dphifdotplusdc_num<<"\n";
	std::cout<<"dphifdotplusdCC\n"<<dphifdotplusdCC<<"\ndphifdotplusdCC_num\n"<<dphifdotplusdCC_num<<"\n";
	std::cout<<"\n\nFibronectin degradation\nphifdotmi_1\n"<<phifdotmi_1<<"\nphifdotmi_2\n"<<phifdotmi_2<<"\n";
	std::cout<<"dphifdotminusdrho\n"<<dphifdotminusdrho<<"\ndphifdotminusdrho_num\n"<<dphifdotminusdrho_num<<"\ndphifdotminusdc\n"<<dphifdotminusdc<<"\ndphifdotminusdc_num\n"<<dphifdotminusdc_num<<"\n";
	std::cout<<"dphifdotminusdCC\n"<<dphifdotminusdCC<<"\ndphifdotminusdCC_num\n"<<dphifdotminusdCC_num<<"\n";
*/


	return 0;
}
