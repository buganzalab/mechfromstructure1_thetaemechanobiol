/*

TEST
Local wound problem.
Global derivatives

*/

#include "wound.h"
//#include "solver.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept> 
#include <math.h> 
#include <string>
#include <stdlib.h>     /* srand, rand */
#include <time.h>    

#include <Eigen/Dense> 
using namespace Eigen;

double frand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main(int argc, char *argv[])
{
	/* initialize random seed: */
	srand (time(NULL));
	  
	std::cout<<"\nTest: Local wound problem \n";
	// check that global derivatives are good to go
	double dt = 0.25; // time step in HOURS
	double dt_loc = 1.0; // local time step in [Fraction of global time steps]
	
	//---------------------------------//
	// GLOBAL PARAMETERS
	//
	double rho_phys = 1000.; // [cells/mm^3]
	double c_max = 1.0e-4; // [g/mm3] from tgf beta review, 5e-5g/mm3 was good for tissues
	//
	//---------------------------------//


	//---------------------------------//
	// LOCAL PARAMTERES
	//---------------------------------//
	//
	// fiber alignment
	double tau_omega_c = 5.0; // time constant for angular reorientation of collagen in [hr]
	double tau_omega_f = 5.0; // time constant for angular reorientation of fibronectin [hr]
	//
	// dispersion parameter
	double tau_kappa_c   = 0.5; // time constant for dispersion function [hr]
	double gamma_kappa_c = 2.0; // exponent of the principal stretch ratio for kappa evolution [-]
	double tau_kappa_f   = 0.5; // time constant for dispersion function [hr]
	double gamma_kappa_f = 2.0; // exponent of the principal stretch ratio for kappa evolution [-]
	// 
	// permanent contracture/growth
	double tau_lamdaP_a = 0.5; // time constant for direction a in [hr]
	double tau_lamdaP_s = 0.5; // time constant for direction s in [hr]
	//
	double thick_0  =  4.0; // reference thickness in [mm]
	double phic_vol =  0.0; // volume of collagen associated to healthy collagen fraction, for phic = 1, phic_vol = 0.6 (or 60%)
	double phif_vol =  0.0; // volume ot fibronectin associated to healthy state.
	std::vector<double> local_parameters = {tau_omega_c,tau_omega_f,tau_kappa_c,gamma_kappa_c,tau_kappa_f,gamma_kappa_f, tau_lamdaP_a, tau_lamdaP_s, thick_0, phic_vol, phif_vol};
	//
	// other local stuff
	double PIE = 3.14159;
	//---------------------------------//
	
	//-------------------------------------------------------------------------------//	

	//---------------------------------//
	// RESULTS
	//---------------------------------//

	// create the variables that you are going to pass
	double phic,kappac,kc;
	Vector2d a0c;
	double phif,kappaf,kf;
	Vector2d a0f;
	Vector2d lamdaP;

	// create the vectors for the derivatives with respect to the global variables (for next test)
	VectorXd dThetadCC(48);
	VectorXd dThetadrho(12);
	VectorXd dThetadc(12);
	
	// NUMERICAl tangent local wrt global parameters
	// Test different conditions. 
	// define the state of the wound in terms of the global variables c, rho and FF
	double c = c_max*frand(0.,1.) ;// concentration of chemical. TGF-beta
	double rho = rho_phys*frand(0.,1.); // concentration of cell population,
	Matrix2d FF; FF<<1.0*frand(0.9,2.),1.0*frand(0.,1.),1.*frand(0.,1.),1.00*frand(0.9,2.); // deformation gradient. Identity
	Matrix2d Identity;Identity<<1.0,0.,0.,1.0;
	Matrix2d CC = FF.transpose()*FF; // right Cauchy-Green deformation tensor
	
	// test different values of the structural parameters
	// define the current state of the microstructure
	double phic_0 = 1.*frand(0.,1.) ;// the current amount of collagen, also how to estimate a good number and then, do you want to make it non-dimensional? why or why not?
	double kc_0 = 0.015; // [MPa], stiffness of the collagen fibers at the current time point, cells can stiffen the fibers by crosslinking, making them larger in diam
	double kappac_0 = 0.5*frand(0.,1.); // the current fiber dispersion, easy to interpret since it doesn't have dimensions
	Vector2d a0c_0(1.,0); // current fiber direction
	//
	double phif_0 = 1.*frand(0.,1.) ;// the current amount of fibronectin, also how to estimate a good number and then, do you want to make it non-dimensional? why or why not?
	double kf_0 = 0.001; // [MPa], stiffness of the fibronectin fibers at the current time point, cells can stiffen the fibers by crosslinking, making them larger in diam
	double kappaf_0 = 0.5*frand(0.,1.); // the current fiber dispersion, easy to interpret since it doesn't have dimensions
	Vector2d a0f_0(1.,0); // current fiber direction for fibronectin
	//
	Vector2d lamdaP_0(0.9,0.9); // current growth parameters
	
	// print the initial state
	std::cout<<"Global variables:\nFF = [["<<FF(0,0)<<","<<FF(0,1)<<"],["<<FF(1,0)<<","<<FF(1,1)<<"]]\nrho = "<<rho<<"\nc = "<<c<<"\n";
	std::cout<<"\nLocal variables:\nphic_0 = "<<phic_0<<"\nkc_0 = "<<kc_0<<"\nkappac_0 = "<<kappac_0<<"\na0c_0 = ["<<a0c_0(0)<<","<<a0c_0(1)<<"]\n";
	std::cout<<"phif_0 = "<<phif_0<<"\nkf_0 = "<<kf_0<<"\nkappaf_0 = "<<kappaf_0<<"\na0f_0 = ["<<a0f_0(0)<<","<<a0f_0(1)<<"]\n";
	std::cout<<"lamdaP_0 = ["<<lamdaP_0(0)<<","<<lamdaP_0(1)<<"]\n";


	// call the subroutine
	localWoundProblem(dt, dt_loc, local_parameters, c, rho, CC,  phic_0, kc_0, a0c_0,  kappac_0,  phif_0, kf_0, a0f_0, kappaf_0, lamdaP_0,phic,kc, a0c,kappac, phif,kf,a0f,kappaf, lamdaP,
		dThetadCC, dThetadrho, dThetadc);


	//
	std::cout<<"\nLocal variables:\nphic = "<<phic<<"\nkc = "<<kc<<"\nkappac = "<<kappac<<"\na0c = ["<<a0c(0)<<","<<a0c(1)<<"]\n";
	std::cout<<"phif = "<<phif<<"\nkf = "<<kf<<"\nkappaf = "<<kappaf<<"\na0f = ["<<a0f(0)<<","<<a0f(1)<<"]\n";
	std::cout<<"lamdaP = ["<<lamdaP(0)<<","<<lamdaP(1)<<"]\n\n";

	// NUMERICA derivative
	double eps = 1e-8;
	//
	double phic_p=0.0,phic_m=0.0;
	double kc_p=0.0,kc_m=0.0;
	double kappac_p=0.0,kappac_m=0.0;
	Vector2d a0c_p,a0c_m;a0c_p.setZero();a0c_m.setZero();
	//
	double phif_p=0.0,phif_m=0.0;
	double kf_p=0.0,kf_m=0.0;
	double kappaf_p=0.0,kappaf_m=0.0;
	Vector2d a0f_p,a0f_m;a0f_p.setZero();a0f_m.setZero();
	//
	Vector2d lamdaP_p,lamdaP_m; lamdaP_p.setZero();lamdaP_m.setZero();
	VectorXd dThetadCC_junk(48);
	VectorXd dThetadrho_junk(12);
	VectorXd dThetadc_junk(12);
	
	// rho
	double rho_plus = rho+eps;
	double rho_minus = rho-eps;
	localWoundProblem(dt,dt_loc,local_parameters,c,rho_plus,CC,phic_0,kc_0,a0c_0,kappac_0,phif_0,kf_0, a0f_0,kappaf_0,lamdaP_0,phic_p,kc_p,a0c_p,kappac_p,phif_p,kf_p,a0f_p,kappaf_p,lamdaP_p,
		dThetadCC_junk, dThetadrho_junk, dThetadc_junk);	
	localWoundProblem(dt,dt_loc,local_parameters,c,rho_minus,CC,phic_0,kc_0,a0c_0,kappac_0,phif_0,kf_0, a0f_0,kappaf_0,lamdaP_0,phic_m,kc_m,a0c_m,kappac_m,phif_m,kf_m,a0f_m,kappaf_m,lamdaP_m,
		dThetadCC_junk, dThetadrho_junk, dThetadc_junk);	
	// derivative
	VectorXd dThetadrho_num(12);
	dThetadrho_num(0) = (1./(2.*eps))*(phic_p - phic_m);
	dThetadrho_num(1) = (1./(2.*eps))*(kc_p - kc_m);
	dThetadrho_num(2) = (1./(2.*eps))*(a0c_p(0) - a0c_m(0));
	dThetadrho_num(3) = (1./(2.*eps))*(a0c_p(1) - a0c_m(1));
	dThetadrho_num(4) = (1./(2.*eps))*(kappac_p - kappac_m);
	//
	dThetadrho_num(5) = (1./(2.*eps))*(phif_p - phif_m);
	dThetadrho_num(6) = (1./(2.*eps))*(kf_p - kf_m);
	dThetadrho_num(7) = (1./(2.*eps))*(a0f_p(0) - a0f_m(0));
	dThetadrho_num(8) = (1./(2.*eps))*(a0f_p(1) - a0f_m(1));
	dThetadrho_num(9) = (1./(2.*eps))*(kappaf_p - kappaf_m);
	//
	dThetadrho_num(10) = (1./(2.*eps))*(lamdaP_p(0) - lamdaP_m(0));
	dThetadrho_num(11) = (1./(2.*eps))*(lamdaP_p(1) - lamdaP_m(1));	
	
	
	// c
	double c_plus = c+eps;
	double c_minus = c-eps;
	localWoundProblem(dt,dt_loc,local_parameters,c_plus,rho,CC,phic_0,kc_0,a0c_0,kappac_0,phif_0,kf_0, a0f_0,kappaf_0,lamdaP_0,phic_p,kc_p,a0c_p,kappac_p,phif_p,kf_p,a0f_p,kappaf_p,lamdaP_p,
		dThetadCC_junk, dThetadrho_junk, dThetadc_junk);	
	localWoundProblem(dt,dt_loc,local_parameters,c_minus,rho,CC,phic_0,kc_0,a0c_0,kappac_0,phif_0,kf_0, a0f_0,kappaf_0,lamdaP_0,phic_m,kc_m,a0c_m,kappac_m,phif_m,kf_m,a0f_m,kappaf_m,lamdaP_m,
		dThetadCC_junk, dThetadrho_junk, dThetadc_junk);	
	// derivative
	VectorXd dThetadc_num(12);
	dThetadc_num(0) = (1./(2.*eps))*(phic_p - phic_m);
	dThetadc_num(1) = (1./(2.*eps))*(kc_p - kc_m);
	dThetadc_num(2) = (1./(2.*eps))*(a0c_p(0) - a0c_m(0));
	dThetadc_num(3) = (1./(2.*eps))*(a0c_p(1) - a0c_m(1));
	dThetadc_num(4) = (1./(2.*eps))*(kappac_p - kappac_m);
	//
	dThetadc_num(5) = (1./(2.*eps))*(phif_p - phif_m);
	dThetadc_num(6) = (1./(2.*eps))*(kf_p - kf_m);
	dThetadc_num(7) = (1./(2.*eps))*(a0f_p(0) - a0f_m(0));
	dThetadc_num(8) = (1./(2.*eps))*(a0f_p(1) - a0f_m(1));
	dThetadc_num(9) = (1./(2.*eps))*(kappaf_p - kappaf_m);
	//
	dThetadc_num(10) = (1./(2.*eps))*(lamdaP_p(0) - lamdaP_m(0));
	dThetadc_num(11) = (1./(2.*eps))*(lamdaP_p(1) - lamdaP_m(1));	
	
	
	// CC
	VectorXd dThetadCC_num(48);dThetadCC_num.setZero();
	Matrix2d CC_plus, CC_minus;
	std::vector<Vector2d> Ebasis(2,Vector2d(0.0,0.0)); Ebasis[0](0)=1.0; Ebasis[1](1)=1.0;
	for(int coordk=0;coordk<2;coordk++){
		for(int coordl=0;coordl<2;coordl++){
			CC_plus = CC + eps*Ebasis[coordk]*Ebasis[coordl].transpose() +  eps*Ebasis[coordl]*Ebasis[coordk].transpose();
			CC_minus = CC - eps*Ebasis[coordk]*Ebasis[coordl].transpose() -  eps*Ebasis[coordl]*Ebasis[coordk].transpose();
			localWoundProblem(dt,dt_loc,local_parameters,c,rho,CC_plus,phic_0,kc_0,a0c_0,kappac_0,phif_0,kf_0, a0f_0,kappaf_0,lamdaP_0,phic_p,kc_p,a0c_p,kappac_p,phif_p,kf_p,a0f_p,kappaf_p,lamdaP_p,
				dThetadCC_junk, dThetadrho_junk, dThetadc_junk);
			localWoundProblem(dt,dt_loc,local_parameters,c,rho,CC_minus,phic_0,kc_0,a0c_0,kappac_0,phif_0,kf_0, a0f_0,kappaf_0,lamdaP_0,phic_m,kc_m,a0c_m,kappac_m,phif_m,kf_m,a0f_m,kappaf_m,lamdaP_m,
				dThetadCC_junk, dThetadrho_junk, dThetadc_junk);
			//
			if(coordk==coordl){
				dThetadCC_num(0*4+coordk*2+coordl) = (1./(4.*eps))*(phic_p - phic_m);	
				dThetadCC_num(1*4+coordk*2+coordl) = (1./(4.*eps))*(kc_p - kc_m);	
				dThetadCC_num(2*4+coordk*2+coordl) = (1./(4.*eps))*(a0c_p(0) - a0c_m(0));	
				dThetadCC_num(3*4+coordk*2+coordl) = (1./(4.*eps))*(a0c_p(1) - a0c_m(1));	
				dThetadCC_num(4*4+coordk*2+coordl) = (1./(4.*eps))*(kappac_p - kappac_m);	
				//
				dThetadCC_num(5*4+coordk*2+coordl) = (1./(4.*eps))*(phif_p - phif_m);	
				dThetadCC_num(6*4+coordk*2+coordl) = (1./(4.*eps))*(kf_p - kf_m);	
				dThetadCC_num(7*4+coordk*2+coordl) = (1./(4.*eps))*(a0f_p(0) - a0f_m(0));	
				dThetadCC_num(8*4+coordk*2+coordl) = (1./(4.*eps))*(a0f_p(1) - a0f_m(1));	
				dThetadCC_num(9*4+coordk*2+coordl) = (1./(4.*eps))*(kappaf_p - kappaf_m);	
				//
				dThetadCC_num(10*4+coordk*2+coordl) = (1./(4.*eps))*(lamdaP_p(0) - lamdaP_m(0));	
				dThetadCC_num(11*4+coordk*2+coordl) = (1./(4.*eps))*(lamdaP_p(1) - lamdaP_m(1));	
			}else{
				dThetadCC_num(0*4+coordk*2+coordl) = (1./(2.*eps))*(phic_p - phic_m);	
				dThetadCC_num(1*4+coordk*2+coordl) = (1./(2.*eps))*(kc_p - kc_m);	
				dThetadCC_num(2*4+coordk*2+coordl) = (1./(2.*eps))*(a0c_p(0) - a0c_m(0));	
				dThetadCC_num(3*4+coordk*2+coordl) = (1./(2.*eps))*(a0c_p(1) - a0c_m(1));	
				dThetadCC_num(4*4+coordk*2+coordl) = (1./(2.*eps))*(kappac_p - kappac_m);	
				//
				dThetadCC_num(5*4+coordk*2+coordl) = (1./(2.*eps))*(phif_p - phif_m);	
				dThetadCC_num(6*4+coordk*2+coordl) = (1./(2.*eps))*(kf_p - kf_m);	
				dThetadCC_num(7*4+coordk*2+coordl) = (1./(2.*eps))*(a0f_p(0) - a0f_m(0));	
				dThetadCC_num(8*4+coordk*2+coordl) = (1./(2.*eps))*(a0f_p(1) - a0f_m(1));	
				dThetadCC_num(9*4+coordk*2+coordl) = (1./(2.*eps))*(kappaf_p - kappaf_m);	
				//
				dThetadCC_num(10*4+coordk*2+coordl) = (1./(2.*eps))*(lamdaP_p(0) - lamdaP_m(0));	
				dThetadCC_num(11*4+coordk*2+coordl) = (1./(2.*eps))*(lamdaP_p(1) - lamdaP_m(1));	
			}
		}
	}
	
	MatrixXd comparedThetadrho(12,3);
	MatrixXd comparedThetadc(12,3);
	MatrixXd comparedThetadCC(48,3);
	for(int i=0;i<12;i++){
		comparedThetadrho(i,0) = i;
		comparedThetadrho(i,1) = dThetadrho(i);
		comparedThetadrho(i,2) = dThetadrho_num(i);
		comparedThetadc(i,0) = i;
		comparedThetadc(i,1) =dThetadc(i);
		comparedThetadc(i,2) =dThetadc_num(i);
	}
	for(int i=0;i<48;i++){
		comparedThetadCC(i,0) = i;
		comparedThetadCC(i,1) = dThetadCC(i);
		comparedThetadCC(i,2) = dThetadCC_num(i);
	}

	std::cout<<"\nrho derivatives\ndThetadrho versus dThetadrho_num:\n"<<comparedThetadrho<<"\n";
	std::cout<<"\nc derivatives\ndThetadc versus dThetadc_num:\n"<<comparedThetadc<<"\n";
	std::cout<<"\nCC derivatives\ndThetadCC versus dThetadCC_num:\n"<<comparedThetadCC<<"\n";
	//

	
}
