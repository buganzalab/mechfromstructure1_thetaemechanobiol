# Wound healing simulations with mechanics evolving according to collagen and fibrin content (1st approach)

This code includes an FEA solver for healing bio-chemo-mechanics in a circular wound that is embedded in a square skin patch.

The work is based on 2017 paper on [CMAME](https://www.sciencedirect.com/science/article/abs/pii/S0045782516302857), and led to the manuscript that is currently archived at [URL].

This code underlies Figure 6a-g in the manuscript and it is the 2nd out of 4 codes that are part of this project.

## Description

Here we consider a first, simplistic link between wound mechanical parameters and its microstructure (fibrin and collagen).
Both fibrin and collagen affect the tissue mechanics linearly.

We also consider a popular approach to represent soft tissue mechanobiology, where elastic tissue stretching enhances the production of cells and collagens.

For a full description of the modeling approach and the obtained results, we refer you to the manuscript currently archived at [URL].

## Getting started

This code is ready for use on Purdue Brown cluster using the job submission file "submit_job.sh", and it will:

1. make the application that solves the wound healing problem

2. solve the healing problem according to the provided input parameters

3. generate vtk files for visualization of the solution, e.g. using ParaView

4. postprocess the data to provide time evolution of multiple variables using the file 'postprocessor.py'

To run it on your machine, make sure to set the correct library path in the makefile and edit the file "submit_job.sh" to your needs.

This code runs with $\Omega_{\phi}^b=2.5$ by default. To generate the other curves shown in Figure 6a-g in the manuscript ($\Omega_{\phi}^b=1.25$ and $\Omega_{\phi}^b=5.00$), you will need to change the multiplier on line 172 in /apps/sim_wound_tri_sym.cpp and re-run the job submission script.

## Authors and acknowledgment
This code was developed in 2018-2022 by Marco Pensalfini (then at ETH Zurich, Switzerland) and Adrian Buganza-Tepole (Purdue University, USA).

The work started in the context of Marco's research visit at Purdue, supported by a Doc.Mobility Fellowship from the Swiss National Science Foundation [REF].
