/* tissue struct file */


#ifndef tissueTri_h
#define tissueTri_h

#include <vector>
#include <map>
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
using namespace Eigen;

//------------------------------------------------------------------------------------//
// Last edit on 22 August 2018 by Marco Pensalfini
// EDIT: added the following to myTissue:
//			- current deformation gradient (ip_FF)
//			- deformation gradient for in vivo state (ip_FF_NS)
//			- deformation gradient for just wounded state (ip_FF_d0wound)
//			- cauchy stresses (ip_sig_all)
//------------------------------------------------------------------------------------//

// Structure for the problem
struct tissue{

	// connectivity (topology)
	int n_node;
	int n_tri;
	int n_IP;
	std::vector<std::vector<int> > Tri;

	// reference geometry
	//
	// nodal values
	std::vector<Vector2d> node_X;
	std::vector<double> node_rho_0;
	std::vector<double> node_c_0;
	std::vector<double> node_p_0;
	//
	// integration point values
	// order by element then by integration point of the element
	// collagen
	std::vector<double> ip_phic_0;
	std::vector<double> ip_mu_0;
	std::vector<double> ip_kc_0;
	std::vector<Vector2d> ip_a0c_0;
	std::vector<double> ip_kappac_0;
	// fibronectin?
	std::vector<double> ip_phif_0;
	std::vector<double> ip_kf_0;
	std::vector<Vector2d> ip_a0f_0;
	std::vector<double> ip_kappaf_0;
	// growth
	std::vector<Vector2d> ip_lamdaP_0;
	// elastic green-lagrange strains
	std::vector<Matrix2d> ip_FF_NS; // check... 
	std::vector<Matrix2d> ip_FF_d0wound; // check...
	
	// deformed geometry
	//
	// nodal values
	std::vector<Vector2d> node_x_prev; // check... 
	std::vector<Vector2d> node_x;
	std::vector<double> node_rho;
	std::vector<double> node_c;
	std::vector<double> node_p;
	//
	// integration point values
	// collagen
	std::vector<double> ip_phic;
	std::vector<double> ip_mu;
	std::vector<double> ip_kc;
	std::vector<Vector2d> ip_a0c;
	std::vector<double> ip_kappac;
	// fibronectin
	std::vector<double> ip_phif;
	std::vector<double> ip_kf;
	std::vector<Vector2d> ip_a0f;
	std::vector<double> ip_kappaf;	
	// growth
	std::vector<Vector2d> ip_lamdaP;
	// elastic green-lagrange strains
	std::vector<Matrix2d> ip_FF; // check ...
	// cauchy stresses
	std::vector<Matrix2d> ip_sig_all; // check ...

	// boundary conditions
	//
	// essential boundary conditions for displacement
	std::map<int,double>  eBC_x;
	// essential boundary conditions for concentrations
	std::map<int,double>  eBC_rho;
	std::map<int,double>  eBC_c;
	std::map<int,double>  eBC_p;
	//
	// traction boundary conditions for displacements
	std::map<int,double> nBC_x;
	// traction boundary conditions for concentrations
	std::map<int,double> nBC_rho;
	std::map<int,double> nBC_c;
	std::map<int,double> nBC_p;
	
	// degree of freedom maps
	//
	// displacements
	std::vector< int > dof_fwd_map_x;
	std::vector< int > dof_fwd_map_xx; // check... 
	//
	// concentrations
	std::vector< int > dof_fwd_map_rho;
	std::vector< int > dof_fwd_map_c;
	std::vector< int > dof_fwd_map_p;
	
	// all dof inverse map
	std::vector< std::vector<int> > dof_inv_map;
	std::vector<int> dof_inv_map_xx;
	std::vector<int> dof_inv_map_p;
	
	// material parameters
	std::vector<double> global_parameters;
	std::vector<double> local_parameters;
	
	// internal element constant (jacobians at IP)
	std::vector<Matrix2d> elem_jac_IP;
	
	// parameters for the simulation
	int n_dof;
	int n_dof_xx;
	int n_dof_p;
	double time_final;
	double time_step;
	double local_time_step; // defined as a fraction of time step
	double time;
	double tol;
	int max_iter;
	int max_time_step_doublings;
	double load_step;
	double load;
	int max_load_step_doublings;
	
};





#endif
