/* post processing functions */

#ifndef utilitiesTri_h
#define utilitiesTri_h

#include "tissue_tri.h" 
#include <vector>
#include <map>
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
#include <Eigen/Sparse> // functions for solution of linear systems
#include <Eigen/OrderingMethods>
//#include<Eigen/StdVector> // to deal with std::vector<Vector2d>

typedef Eigen::SparseMatrix<double> SpMat; // declares a column-major sparse matrix type of double
typedef Eigen::Triplet<double> T;

using namespace Eigen;


//-------------------------------------------------//
// IO
//-------------------------------------------------//

//----------------------------//
// READ ABAQUS
//----------------------------//
//
// read in an Abaqus input file with mesh and fill in structures
//void readAbaqusInput(const char* filename, tissue &myTissue);

//----------------------------//
// READ COMSOL edited
//----------------------------//
//
// read in an Comsol input file with mesh and fill in structures
void readComsolEditInput(const char* filename,tissue &myTissue);

//----------------------------//
// REAd OWN FILE
//----------------------------//
//
tissue readTissue(const char* filename);

//----------------------------//
// WRITE PARAVIEW
//----------------------------//
//
// write the paraview file with 
//	RHO, C, PHI, THETA_B at the nodes
void writeParaviewBiol(tissue &myTissue, const char* filename, double res);
void writeParaviewMech(tissue &myTissue, const char* filename, double res);
void writeParaview(int FLAG, tissue &myTissue, const char* filename );

//----------------------------//
// WRITE OWN FILE
//----------------------------//
//
void writeTissue(tissue &myTissue, const char* filename,double time, double res);
// Note: what am I passing a time variable?!

//----------------------------//
// WRITE NODE
//----------------------------//
//
// write the node information
// DEFORMED x, RHO, C
void writeNode(tissue &myTissue,const char* filename,int nodei,double time);
// Note: what am I passing a time variable?!

//----------------------------//
// WRITE IP
//----------------------------//
//
// write integration point information
// just ip variables
// PHI A0 KAPPA LAMBDA 
void writeIP(tissue &myTissue,const char* filename,int ipi,double time);

//----------------------------//
// WRITE ELEMENT
//----------------------------//
//
// write an element information to a text file. write the average
// of variables at the center in the following order
//	PHI A0 KAPPA LAMDA_B RHO C
void writeElement(tissue &myTissue,const char* filename,int elemi,double time);

//----------------------------//
// PROJECT TO NODES
//----------------------------//
//
// For information computed at the integration point, project to the nodes
void projectCC2nodes(const tissue &myTissue, VectorXd &CCnodes_consistent, VectorXd &CCnodes_lumped);

#endif